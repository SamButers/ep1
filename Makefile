BINFOLDER := bin/
INCFOLDER := inc/
SRCFOLDER := src/
OBJFOLDER := obj/

CC := g++

CFLAGS := -W -Wall -ansi -pedantic

SRCFILES := $(wildcard src/*.cpp)

all: create_folders $(SRCFILES:src/%.cpp=obj/%.o)
	$(CC) $(CFLAGS) obj/*.o -o bin/executable

obj/%.o: src/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ -I./inc

create_folders:
	@mkdir -p $(OBJFOLDER) $(BINFOLDER)
run: bin/executable
	cd bin && ./executable

.PHONY: clean
clean:
	rm -rf obj/*
	rm -rf bin/*
