# Exercício de Programação 1

Access the complete description of this exercise in the [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao).

## How to use the project

To compile the project, simply use the make command in the terminal.
To run the project, simply use the make run command in the terminal.

After finishing running the program, a report file will be created in the main directory by the name "report.txt". Open it to check the results.

## Project's functionalities

The project contains five classes: pessoa, candidato, eleitor, urna and voto.

### pessoa class

The pessoa class contains two attributes: name and cpf.

The name attribute contains the name of a person. The cpf attribute contains the CPF of a person.

Each attribute contains its get and set methods for element access: getName, getCpf, setName and setCpf.

This class serves as base for the following classes: candidato and eleitor.

### candidato class

The candidato class is a derived class from pessoa and contains six attributes, alongside the inherited attributes. Its five attributes are: number, party, role, status, local and voteNumber.

The number attribute contains the candidate's number, which is used for voting. The party attribute contains the party from which the candidate is from. The role attribute contains the role the candidate is contesting for, such as president or deputy. The status attribute contains the state of the candidate, which is used to decide if they're able to receive votes or not. The local attribute contains where the candidate is contesting for. The voteNumber attribute contains the number of votes the candidate has received.

Each attribute contains its get and set methods for element access that follows the following pattern: getAttributename and setAttributename. The attribute name shall have its first letter capitalized.

This class has a constructor method that will receive the following parameters in order: name, cpf, number, party, role, status and local.

### eleitor class

The eleitor class is a derived class from pessoa and contains two attributes, aside from the inherited attributes. Its attributes are *elVote[5] and roleNumber.

The *elVote[5] is a vector of pointers for a voto object, which carries informations about the eleitor's vote.

The roleNumber contains the information about how many votes the eleitor class has. The roleNumber contains its set and get methods.

This class also has four methods, aside from the inherited ones. Its four methods are: vote, vote, getRoleNumber and setRoleNumber. The vote methods are used to initialize and construct the eleitor's elVote object. The first receives a candidato pointer and the second a short int.

This class has a constructor method that will receive the following parameters in order: name and cpf.

### urna class

The urna class contains eleven attributes: maxVotes, countNumber, check, eCheck, c, x, electVector, elName, elCpf, candNumber and reportFile.

The attributes countNumber, check, eCheck, c, and x are used mainly as conditions in loops and as counters. The maxVotes contains the number of electors that will vote in the session. The electVector is a vector that contains multiple eleitor objects. The elName, elCpf and candNumber are attributes used to temporarily store information from the electors. The reportFile is a ofstream object responsible for the report output.

This class also has twelve methods. Its methods are: startSession, generateReport, getMaxVotes, setMaxVotes, electorIdentification, electorCheck, electorVoting, candNumberCheck, candidateScan, candidatePrinting, getCandRole and generalCheck.

The startSession is responsible for starting the voting session. The generateReport is responsible for generating the report. getMaxVotes and setMaxVotes are responsible for accessing the maxVotes attributes. The electorIdentification is responsible for identifying the elector. The electorCheck is responsible for checking if the elector informations are valid. The electorVoting is responsible for the voting. The candNumberCheck is responsible for validating the candidate number. The candidateScan is responsible for scanning the candidate vector for the proper candidate of the number inputted. The candidatePrinting is responsible for printing the chosen candidate information and the voting options. The getCandRole is responsible for getting the current role that the elector is supposed to vote on. The generalCheck is a generic check for strings that shall only contain numbers.

This class has a constructor method that will receive the number of electors for the session.

### voto class

The voto class contains two attributes: target and voteType.

The target attribute contains a pointer for the candidate that was voted on, if a candidate has received the vote. The voteType attribute contains the type of the vote, if it was for a candidate, "BRANCO" or null.

This class has two constructor methods. One of them will receive a candidato object and the other one will receive a short int for the voteType. The former shall be used in case a candidate exists and is valid for vote, and the latter in the other cases.

## Bugs and glitches

## References
