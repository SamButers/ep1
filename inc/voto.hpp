#ifndef VOTO_H
#define VOTO_H

#include <iostream>
#include <string>
#include "candidato.hpp"

using namespace std;

class voto{
	public:
	voto(candidato &);
	voto(short int);

	short int getVoteType();
	string getCandidato();
	void setVoteType(short int);

	private:
	candidato *target;
	short int voteType;

	void setCandidato(candidato &);

};

#endif
