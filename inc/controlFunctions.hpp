void stringChange(string &a, char *begin, char *end){
	string b;
	while(begin != end){
		b.push_back(*begin);
		begin++;
	}
	a.erase();
	a.replace(0, 0, b);
}

void setVariables(string infoString, string &name, string &cpf, string &number, string &party, string &role, string &status, string &local){
	int c, controlV, occurrenceV;
	c = 0;
	controlV = 0;
	occurrenceV = 0;

	while(infoString[c] != '\0'){
		if(infoString[c] == ';'){

		switch(occurrenceV){
		case 12:
			stringChange(local, &infoString[controlV + 2], &infoString[c - 1]);
			break;
		case 14:
			stringChange(role, &infoString[controlV + 2], &infoString[c - 1]);
			break;
		case 16:
			stringChange(number, &infoString[controlV + 2], &infoString[c - 1]);
			break;
		case 18:
			stringChange(name, &infoString[controlV + 2], &infoString[c - 1]);
			break;
		case 20:
			stringChange(cpf, &infoString[controlV + 2], &infoString[c - 1]);
                        break;
		case 23:
			stringChange(status, &infoString[controlV + 2], &infoString[c - 1]);
                        break;
		case 29:
			stringChange(party, &infoString[controlV + 2], &infoString[c - 1]);
			break;
		}
		controlV = c;
		occurrenceV++;
		}

		if(occurrenceV == 30)
			break;
		c++;
	}
}
