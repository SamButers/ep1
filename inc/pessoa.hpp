#ifndef PESSOA_H
#define PESSOA_H

#include <iostream>
#include <string>

using namespace std;

class pessoa{
	public:
	void setName(string);
	void setCpf(string);
	string getCpf();
	string getName();

	protected:
	string name, cpf;

};

#endif
