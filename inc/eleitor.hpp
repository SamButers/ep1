#ifndef ELEITOR_H
#define ELEITOR_H

#include "pessoa.hpp"
#include <iostream>
#include <string>
#include "voto.hpp"

using namespace std;

class eleitor: public pessoa{
	public:
	voto *elVote[5];

	eleitor(string, string);

	void vote(candidato *);
	void vote(short int);

	short int getRoleNumber();

	private:
	short int roleNumber;

	void setRoleNumber(short int);

};

#endif
