#ifndef URNA_H
#define URNA_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "candidato.hpp"
#include "eleitor.hpp"

using namespace std;

class urna{
	public:
	urna(long int);

	void startSession(vector<candidato*> &);
	long int getMaxVotes();
	void generateReport(vector<candidato*> &);

	private:
	long int maxVotes, countNumber, check, eCheck;
	short int c, x;
	vector<eleitor*> electVector;
	string elName, elCpf, candNumber;
	ofstream reportFile;

	void setMaxVotes(long int maxVotes);
	void electorIdentification();
	bool electorCheck();
	void electorVoting(vector<candidato*> &);
	bool candNumberCheck();
	void candidateScan(vector<candidato*> &);
	short int candidatePrinting(vector<candidato*> &);
	string getCandRole();
	bool generalCheck(int, string&);
};

#endif
