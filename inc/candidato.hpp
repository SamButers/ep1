#ifndef CANDIDATO_H
#define CANDIDATO_H

#include <iostream>
#include <string>
#include "pessoa.hpp"

using namespace std;

class candidato: public pessoa{
	public:
	candidato(string, string, string, string, string, string, string);

	string getNumber();
	string getParty();
	string getRole();
	string getLocal();
	void setNumber(string);
	void setParty(string);
	void setRole(string);
	string getStatus();
	void setStatus(string);
	void setLocal(string);
	void setVoteNumber(unsigned long int);
	unsigned long int getVoteNumber();

	private:
	string number, party, role, status, local;
	unsigned long int voteNumber;

};


#endif
