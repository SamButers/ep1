#include "urna.hpp"

urna::urna(long int maxVotes){
	setMaxVotes(maxVotes);
}

long int urna::getMaxVotes(){
	return maxVotes;
}

void urna::setMaxVotes(long int maxVotes){
	this->maxVotes = maxVotes;
}

void urna::startSession(vector<candidato*> & candVector){
	for(countNumber = 0; countNumber < maxVotes; countNumber++){
		electorIdentification();
		electorVoting(candVector);
	}
}

void urna::electorIdentification(){
	invalidCpf:
	cout << "Insira o seu nome: ";
	cin.ignore(9999, '\n');
	getline(cin, elName);
	cout << "Insira o seu CPF: ";
	cin >> elCpf;

	if(electorCheck())
	electVector.push_back(new eleitor(elName, elCpf));

	else{
		cout << "CPF inválido." << endl;
		goto invalidCpf;
	}
}

void urna::candidateScan(vector<candidato*> &candVector){
	while(check != candVector.size()){
			if((candNumber.compare(candVector[check]->getNumber()))||!(getCandRole() == candVector[check]->getRole()))
				check++;
			else
				break;
		}
}

short int urna::candidatePrinting(vector<candidato*> &candVector){
	if(check >= candVector.size()){
			wrongoption:
			cout << "Candidato: NULO." << endl << "Digite CONFIRMAR para confirmar o seu voto ou digite CANCELAR para escolher outro número: ";
			cin >> candNumber;

			if(candNumber == "CONFIRMAR")
				electVector[countNumber]->vote(2);

			else{
				if(candNumber == "CANCELAR")
					return 0;
				else
					goto wrongoption;
			}

	}

	else{
		if(candVector[check]->getStatus() == "INAPTO"){
			cout << "INAPTO." << endl;
			return 0;
		}

		notright:
		cout << "Candidato: " << candVector[check]->getName() << endl << "Cargo: " << candVector[check]->getRole() << endl << "Número: " << candVector[check]->getNumber() << endl << "Partido: " << candVector[check]->getParty() << endl << "Digite CONFIRMAR para confirmar o seu voto ou digite CANCELAR para escolher outro número: ";
		cin >> candNumber;

		if(candNumber == "CONFIRMAR")
				electVector[countNumber]->vote(candVector[check]);

			else{
				if(candNumber == "CANCELAR")
					return 0;
				else
					goto notright;
			}

		}

	return 1;
}

void urna::electorVoting(vector<candidato*> &candVector){
	for(c = 0; c < 5; c++){
			vote:
			check = 0;

			cout << "Insira o número do seu candidato para " << getCandRole() << " ou digite BRANCO para votar em branco: ";
			cin >> candNumber;

			if(candNumber == "BRANCO")
				electVector[countNumber]->vote(1);

			else{
				if(candNumberCheck()){
				candidateScan(candVector);
				if(candidatePrinting(candVector) == 0)
					goto vote;
				}

				else
					goto vote;
			}
	}
}

string urna::getCandRole(){
	switch(c){

	case 0:
		return "DEPUTADO DISTRITAL";
		break;

	case 1:
		return "DEPUTADO FEDERAL";
		break;

	case 2:
		return "SENADOR";
		break;

	case 3:
		return "GOVERNADOR";
		break;

	case 4:
		return "PRESIDENTE";
		break;

	}
}

bool urna::electorCheck(){
	if(elCpf.size() != 11)
		return false;
	else{
		if(generalCheck(11, elCpf)){
			while(eCheck != electVector.size()){
			if(elCpf.compare(electVector[eCheck]->getCpf()))
				eCheck++;
			else
				return false;
			}

			return true;
		}

		else
			return false;

	}
}

bool urna::candNumberCheck(){
	switch(c){

	case 0:
		candNumber.resize(5);

		return generalCheck(5, candNumber);

	case 1:
		candNumber.resize(4);

		return generalCheck(4, candNumber);

	case 2:
		candNumber.resize(3);

		return generalCheck(3, candNumber);

	case 3:
		candNumber.resize(2);

		return generalCheck(2, candNumber);

	case 4:
		candNumber.resize(2);

		return generalCheck(2, candNumber);

	}
}

bool urna::generalCheck(int y, string &targetString){
	for(x = 0; x < y; x++){
			switch (targetString[x]){
			case '0':
				break;
			case '1':
				break;
			case '2':
				break;
			case '3':
				break;
			case '4':
				break;
			case '5':
				break;
			case '6':
				break;
			case '7':
				break;
			case '8':
				break;
			case '9':
				break;
			default:
				return false;
				break;
			}
	}

	return true;
}

void urna::generateReport(vector<candidato*> &candVector){
	while(!(reportFile.is_open()))
		reportFile.open("../report.txt");

	reportFile << "ARQUIVO DE RELATÓRIO" << endl << endl;

	check = 0;

	while(check != electVector.size()){
			reportFile << "Nome: " << electVector[check]->getName() << endl << "CPF: " << electVector[check]->getCpf() << endl;
			reportFile << "Votos:" << endl;

			for(c = 0; c < 5; c++)
				reportFile << getCandRole() << ": " << electVector[check]->elVote[c]->getCandidato() << endl;

			reportFile << endl << endl;

			check++;
		}

	reportFile << "Vencedores da eleição: " << endl;

	for(c = 0; c < 5; c++){
		unsigned long int voteN;
		candidato *winner;
		candidato *winner2;
		candidato *supWinner[2];
		bool viceVar, draw;
		string vice, assistSup[2];
		viceVar = true;
		draw = false;
		voteN = 0;
		check = 0;
		vice = "VICE-";

		assistSup[0] = candVector[34]->getRole();
		assistSup[1] = assistSup[0];
		assistSup[1].replace(0, 1, "1");
		supWinner[0] = new candidato("NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL");
		supWinner[1] = supWinner[0];

		while(check != candVector.size()){
			if((candVector[check]->getVoteNumber() > voteN)&&(getCandRole() == candVector[check]->getRole())){
				draw = false;
				voteN = candVector[check]->getVoteNumber();
				winner = candVector[check];

				eCheck = 0;
				vice.append(getCandRole());
				if(getCandRole() != "SENADOR"){
					while(eCheck != candVector.size()){
						if((winner->getNumber() == candVector[eCheck]->getNumber())&&(candVector[eCheck]->getRole() == vice))
							break;
						else
							eCheck++;
					}
				}

				if(getCandRole() == "SENADOR"){
					while(eCheck != candVector.size()){
						if((winner->getNumber() == candVector[eCheck]->getNumber())&&(candVector[eCheck]->getRole() == assistSup[1])&&(candVector[eCheck]->getStatus() != "INAPTO"))
							supWinner[0] = candVector[eCheck];

						if((winner->getNumber() == candVector[eCheck]->getNumber())&&(candVector[eCheck]->getRole() == assistSup[0])&&(candVector[eCheck]->getStatus() != "INAPTO"))
							supWinner[1] = candVector[eCheck];


						eCheck++;
					}
				}

				if(eCheck != candVector.size())
					winner2 = candVector[eCheck];
				else
					viceVar = false;
			}

			else
				if((candVector[check]->getVoteNumber() == voteN)&&(getCandRole() == candVector[check]->getRole())&&(voteN != 0))
					draw = true;

				check++;
		}

		if(voteN == 0)
			reportFile << getCandRole() << ": " << "Não há vencedores. Todos os votos para o cargo de " << getCandRole() << " foram NULO ou BRANCO." << endl << endl;

		if(draw == true)
			reportFile << getCandRole() << ": " << "Não há vencedores. Houve EMPATE para o cargo de " << getCandRole() << "." << endl << endl;

		if((voteN != 0)&&(draw == false)){
			if(viceVar == true)
				reportFile << getCandRole() << ": " << winner->getName() << endl << vice << ": " << winner2->getName() << endl << "Número de votos: " << winner->getVoteNumber() << endl << endl;

			if(getCandRole() == "SENADOR"){
				reportFile << getCandRole() << ": " << winner->getName() << endl;

				if(supWinner[0]->getName() != "NULL")
					reportFile << "1° SUPLENTE: " << supWinner[0]->getName() << endl;

				if(supWinner[1]->getName() != "NULL")
					reportFile << "2° SUPLENTE: " << supWinner[1]->getName() << endl;

				reportFile << "Número de votos: " << winner->getVoteNumber() << endl << endl;
				
			}
				
			if((viceVar == false)&&(getCandRole() != "SENADOR"))
				reportFile << getCandRole() << ": " << winner->getName() << endl << "Número de votos: " << winner->getVoteNumber() << endl << endl;
		}
	}

	reportFile.close();
}
