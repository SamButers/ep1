#include "eleitor.hpp"

eleitor::eleitor(string name, string cpf){
	setName(name);
	setCpf(cpf);
	setRoleNumber(0);
}

void eleitor::vote(candidato * target){
	elVote[roleNumber] = new voto(*target);
	setRoleNumber((getRoleNumber()) + 1);
}

void eleitor::vote(short int voteType){
	elVote[roleNumber] = new voto(voteType);
	setRoleNumber((getRoleNumber()) + 1);
}

short int eleitor::getRoleNumber(){
	return roleNumber;
}

void eleitor::setRoleNumber(short int roleNumber){
	this->roleNumber = roleNumber;
}
