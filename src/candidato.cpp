#include "candidato.hpp"

candidato::candidato(string name, string cpf, string number, string party, string role, string status, string local){
	setName(name);
	setCpf(cpf);
	setNumber(number);
	setParty(party);
	setRole(role);
	setStatus(status);
	setLocal(local);
	setVoteNumber(0);
}

string candidato::getNumber(){
	return number;
}

string candidato::getParty(){
	return party;
}

string candidato::getRole(){
	return role;
}

string candidato::getStatus(){
	return status;
}

string candidato::getLocal(){
    return local;
}

unsigned long int candidato::getVoteNumber(){
	return voteNumber;
}

void candidato::setNumber(string number){
	this->number = number;
}

void candidato::setParty(string party){
	this->party = party;
}

void candidato::setRole(string role){
	this->role = role;
}

void candidato::setStatus(string status){
	this->status = status;
}

void candidato::setLocal(string local){
	this->local = local;
}

void candidato::setVoteNumber(unsigned long int voteNumber){
	this->voteNumber = voteNumber;
}
