#include <iostream>
#include <string>
#include <fstream>
#include "candidato.hpp"
#include <vector>
#include "urna.hpp"
#include "controlFunctions.hpp"

using namespace std;

int main(){
	ifstream candFile;
	vector<candidato*> candVector;
	string candLine, name, cpf, number, party, role, status, local;
	short int c, x;
	x = 0;

//File reading/processing and candidate objects allocation.

	for(c = 0; c < 2; c++){

	if(c == 0){
	while(!(candFile.is_open()))
		candFile.open("../data/consulta_cand_2018_BR.csv");
	}

	if(c == 1){
		while(!(candFile.is_open()))
		candFile.open("../data/consulta_cand_2018_DF.csv");
	}

	if(candFile.is_open()){
		while(candFile){
			getline(candFile, candLine);
		if(x != 0){
			setVariables(candLine, name, cpf, number, party, role, status, local);
			candVector.push_back(new candidato(name, cpf, number, party, role, status, local));
		}
			if(x == 0)
			x++;
		}
	}

	x = 0;
	candFile.close();
	}

//File reading/processing and candidate objects allocation.

//Vote

	long int maxN;

	cout << "Insira o número de eleitores para essa sessão: ";
	cin >> maxN;

	while(cin.fail()){
		cin.clear();
		cout << "Insira o número de eleitores para essa sessão: ";
		cin.ignore(9999, '\n');
		cin >> maxN;
	}


	urna urna1(maxN);

	urna1.startSession(candVector);

//Vote

//Report

	urna1.generateReport(candVector);

//Report

	return 0;
}
