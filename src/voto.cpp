#include "voto.hpp"

voto::voto(candidato & target){
	setCandidato(target);
	setVoteType(0);
	target.setVoteNumber((target.getVoteNumber()) + 1);
}

voto::voto(short int voteType){
	setVoteType(voteType);
}

short int voto::getVoteType(){
	return voteType;
}

string voto::getCandidato(){
	if(voteType == 0)
		return target->getName();
	if(voteType == 1)
		return "BRANCO";
	if(voteType == 2)
		return "NULO";
}

void voto::setVoteType(short int voteType){
	this->voteType = voteType;
}

void voto::setCandidato(candidato & target){
	this->target = &target;
}
